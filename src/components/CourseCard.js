import { Card, Button } from 'react-bootstrap';
import { useState } from 'react';
import { PropTypes } from 'prop-types';
 
export default function CourseCard({courseProps}) {

	/*console.log("Contents of props: ");
	console.log(props);
	console.log(typeof props);*/

	const {name, description, price} = courseProps;

	// State Hooks (useState) - a way to store information within a component and track this information
			// getter, setter
			// variable, function to change the value of a variable
	const [count, setCount] = useState(0);
	const [seat, vacantSeat] = useState(30); // count = 0;

	function enroll(){
		if(seat > 0){
		setCount(count + 1);
		console.log('Enrollees: ' + count);
		vacantSeat(seat - 1);
		console.log('Seats: ' + seat);
		}
		else{
			alert("No seats available!");
	}
}


	return (
	    <Card>
	        <Card.Body>
	            <Card.Title>{name}</Card.Title>
	            <Card.Subtitle>Description:</Card.Subtitle>
	            <Card.Text>{description}</Card.Text>
	            <Card.Subtitle>Price:</Card.Subtitle>
	            <Card.Text>{price}</Card.Text>
	            <Button variant="primary" onClick={enroll}>Enroll</Button>
	            <Card.Text>Total Enrolled: {count}<br/></Card.Text>
	            <Card.Text>Seat/s left: {seat}<br/></Card.Text>
	        </Card.Body>
	    </Card>
	)
}

//  Check if the courseCard component is getting the correct property types
CourseCard.propTypes = {
	courseProps: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}

