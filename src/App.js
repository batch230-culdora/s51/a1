// remove: import logo from './logo.svg';
import './App.css';
import { Container } from 'react-bootstrap';
import { Fragment } from 'react'; /* React Fragments allows us to return multiple elements*/

import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Courses from './pages/Courses'


function App() {
  return (
    /* React Fragments allows us to return multiple elements*/
    <Fragment>
      <AppNavbar />
      <Container>
        <Home />
        <Courses />
      </Container>
    </ Fragment>
  );
}

export default App;
